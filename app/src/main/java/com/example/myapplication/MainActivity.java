package com.example.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private Button button;
    String formattedDate;


    @Override

    protected void onStop() {
        super.onStop();
        // Saving data
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString ("time", formattedDate);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(cal.getTime());
    }

    protected void onPause() {
        super.onPause();
        // Saving data
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString ("time", formattedDate);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(cal.getTime());
    }


    protected void onStart() {
        super.onStart();
        // Retrieve data
        setContentView(R.layout.activity_main);
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, formattedDate, Snackbar.LENGTH_LONG).setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).show();
    }

    protected void onResume () {
        super.onResume();
        // Retrieve data
        setContentView(R.layout.activity_main);
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, formattedDate, Snackbar.LENGTH_LONG).setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ClickMusicPlayer(View v) {
        openMusicPlayer();
    }

    public void openMusicPlayer() {
        Intent intent = new Intent(this, MusicPlayer.class);
        startActivity(intent);
    }

    public void ClickLyrics(View v) {
        openLyrics();
    }

    public void openLyrics() {
        Intent intent = new Intent(this, Lyrics.class);
        startActivity(intent);
    }


    public void ClickFirebase(View v) {
        openFirebase();
    }

    public void openFirebase() {
        Intent intent = new Intent(this, Firebase.class);
        startActivity(intent);
    }


    public void ClickQuit(View v)
    {
       final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
       builder.setMessage("Are you sure you want to quit ?");
       builder.setCancelable(true);
       builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
               dialog.cancel();
           }
       });
       builder.setPositiveButton("Quit!", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
               moveTaskToBack(true);
           }
       });
       AlertDialog alertdialog = builder.create();
       alertdialog.show();
    }
}
