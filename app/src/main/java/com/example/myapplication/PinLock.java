package com.example.myapplication;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.HideType;
import com.nightonke.blurlockview.Directions.ShowType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

public class PinLock extends AppCompatActivity {

    private BlurLockView blurLockView;
    private ImageView imageView;

    public void PasswordCorrect(View v) {
        openMainMenu();
    }

    public void openMainMenu() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_lock);

        blurLockView = (BlurLockView)findViewById(R.id.blurlockview);
        blurLockView.setBlurredView(imageView);

        imageView = (ImageView) findViewById(R.id.backgroundImageView);

        blurLockView.setCorrectPassword("1111");
        blurLockView.setLeftButton("");
        blurLockView.setRightButton("");
        blurLockView.setTypeface(Typeface.DEFAULT);
        blurLockView.setType(Password.NUMBER,false);


        blurLockView.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {
                Toast.makeText(PinLock.this, "Password Correct", Toast.LENGTH_SHORT).show();
                openMainMenu();
            }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(PinLock.this, "Password Inorrect", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void input(String inputPassword) {

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blurLockView.show(1000, ShowType.FADE_IN, EaseType.EaseInOutBack);
            }
        });
    }
}

