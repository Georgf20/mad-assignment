package com.example.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SeekBar;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class MusicPlayer extends AppCompatActivity {

    private DrawerLayout dl;
    private ActionBarDrawerToggle abdt;
    private Button button;
    public int resId;

    Button PauseButton;
    SeekBar volumeBar;
    ListView songsListView;
    ArrayList<String> arrayList;
    ArrayAdapter Adapter;
    MediaPlayer Player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        dl = (DrawerLayout) findViewById(R.id.dl);
        abdt = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);
        dl.addDrawerListener(abdt);
        abdt.syncState();



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);

        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            public void ClickGoToMenu(View v) {
                openMenu();
            }

            public void openMenu() {
                Intent intent = new Intent(MusicPlayer.this, MainActivity.class);
                startActivity(intent);
            }

            public void ClickLyrics(View v) {
                openLyrics();
            }

            public void openLyrics() {
                Intent intent = new Intent(MusicPlayer.this, Lyrics.class);
                startActivity(intent);
            }


            public void ClickFirebase(View v) {
                openFirebase();
            }

            public void openFirebase() {
                Intent intent = new Intent(MusicPlayer.this, Firebase.class);
                startActivity(intent);
            }

            @Override

            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if (id == R.id.GoMainMenu) {
                    openMenu();
                }

                if (id == R.id.GoToLyrics) {
                    openLyrics();
                }

                if (id == R.id.GoToFirebase) {
                    openFirebase();
                }

                if (id == R.id.Back) {
                    moveTaskToBack(true);
                }
                return true;
            }
        });

        songsListView = findViewById(R.id.songsListView);
        arrayList = new ArrayList<String>();
        Field[] fields = R.raw.class.getFields();
        for (int i = 0; i < fields.length; i++) {
            arrayList.add(fields[i].getName());
        }

        Adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList);
        songsListView.setAdapter(Adapter);

        songsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(Player != null) {
                    Player.release();
                }

                resId = getResources().getIdentifier(arrayList.get(position), "raw", getPackageName());
                Player = MediaPlayer.create(MusicPlayer.this,resId);
                PauseButton.setBackgroundResource(R.drawable.pause);
                Player.start();

            }
        });

        volumeBar = (SeekBar) findViewById(R.id.seekBar);
        volumeBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        float volumeNum = progress / 100f;
                        Player.setVolume(volumeNum, volumeNum);
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }
                }
        );

        PauseButton = (Button) findViewById(R.id.PauseButton);

    }

        @Override

        public boolean onOptionsItemSelected (MenuItem item){
            return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
        }

        public void openYt (View view){
            Intent YoutubeIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/"));
            startActivity(YoutubeIntent);
        }

        public void playBtnPause (View view) {
            if(!Player.isPlaying()) {
                Player.start();
                PauseButton.setBackgroundResource(R.drawable.pause);
            } else {
                Player.pause();
                PauseButton.setBackgroundResource(R.drawable.playbutton);
            }
        }

    @Override
    protected void onStart() {
        super.onStart();
        // Retrieve shared preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int sound = prefs.getInt("in",0);
    }

    protected void onResume() {
        super.onResume();
        // Retrieve shared preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int sound = prefs.getInt("in",0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Saving data

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MusicPlayer.this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt("in", resId);
        editor.apply();
    }
    @Override
    protected void onStop() {
        super.onStop();
        // Saving data

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MusicPlayer.this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt("in", resId);
        editor.apply();
    }
}













